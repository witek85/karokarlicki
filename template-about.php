<?php
/**
 * Template Name: O firmie
 */
?>

<?php get_template_part('templates/page', 'header'); ?>

  <div class="wrapper">
    <div class="row row-md-spacing nomargin">
      
      <div class="col-md-8 nopadding content">

      <?php while (have_posts()) : the_post(); ?>
          
        <?php get_template_part('templates/content', 'page'); ?>

      <?php endwhile; ?>
      </div>
      <div class="col-md-4 nopadding sidebar">
        <?php dynamic_sidebar('sidebar-about'); ?>
      </div>
        
    </div>
  </div>