<?php
/**
 * Template Name: Strona główna
 */
?>

<?php get_template_part('templates/homepage', 'slider'); ?>

  <div class="wrapper">
    <div class="row row-md-spacing nomargin">
      
      <div class="col-md-8 nopadding nomargin content">

      <?php while (have_posts()) : the_post(); ?>

        <?php get_template_part('templates/content', 'page'); ?>

      <?php endwhile; ?>
      </div>
      <div class="col-md-4 nopadding nomargin sidebar">
        <?php dynamic_sidebar('sidebar-primary'); ?>
      </div>
        
    </div>
  </div>