<!--<footer class="content-info" >
  <div class="container">
<?php dynamic_sidebar('sidebar-footer'); ?>
  </div>
</footer>-->


<footer class="footer wrapper" role="contentinfo">
  <div class="footer-box">
    <div class="row nomargin">
      <?php dynamic_sidebar('sidebar-footer'); ?>
<!--      <div class="col-md-4"><h2>Na skróty</h2>
        <ul>
          <li><a title="menu" href="#">menu</a></li>
        </ul>
      </div>
     -->
    </div>
  </div>
  <div class="row copyright nomargin">
    <div class="col-md-6 nopadding">
      <p>© Copyright 2016&nbsp;<a href="/" title="Strona główna">karokarlicki.pl</a>&nbsp; | &nbsp;<a href="/polityka-prywatnosci.html" title="Polityka prywatnośc">Polityka prywatności</a>&nbsp; | Wszelkie prawa zastrzeżone.</p>
    </div>
    <div class="col-md-6 nopadding text-right">
      <p>Wykonanie:&nbsp;<a href="http://www.adimo.pl/" target="_blank" title="Adimo.pl - Technologie internetowe">ADIMO.PL</a></p>
    </div>
  </div>
</footer>