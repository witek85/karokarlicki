<?php use Roots\Sage\Titles; ?>

<header class="header">
  <div class="wrapper">
    <h1><?= Titles\title(); ?></h1>
  </div>
</header>