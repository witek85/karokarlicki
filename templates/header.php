<?php
// This file assumes that you have included the nav walker from https://github.com/twittem/wp-bootstrap-navwalker
// somewhere in your theme.
?>

<header>
  <div class="top-bar">
    <div class="wrapper">
      <div class="row align-right nomargin">
        <a href="/" class="above-link">
          <span class="icon-home"></span>
        </a>
        <a href="#" class="above-link">
          <span class="icon-cart"></span>
        </a>
        <a href="mailto:biuro@karokarlicki.pl" class="above-link">
          <span class="icon-envelope"></span>
          <span>kontakt</span>
        </a>
        <span class="above-link menu">
          <span><?php echo pll_current_language('name'); ?></span>
          <span class="icon-down"></span>
          <ul class="sub-menu">
            <?php pll_the_languages();?>
          </ul>
        </span>
      </div>
    </div>
  </div>

  <div class="banner navbar navbar-default navbar-static-top" role="banner">
    <div class="wrapper">
      <div class="navbar-header">
        <a href="<?= esc_url(home_url('/')); ?>" title="Strona główna">
          <div class="logo"></div></a>
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>

      <nav class="collapse navbar-collapse navbar-nav" role="navigation">
        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new wp_bootstrap_navwalker(), 'menu_class' => 'nav navbar-nav']);
        endif;
        ?>
      </nav>

    </div>

  </div>
</div>
</header>